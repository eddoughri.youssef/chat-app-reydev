


export function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      let cookies = document.cookie.split(';');
      for (let i = 0; i < cookies.length; i++) {
        let cookie = cookies[i].trim();
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }
export function deleteCookie(name){
  document.cookie = `${name}=; Max-Age=-1`;
}

export const url = "http://13.37.211.176:3000";
export const url_ws = "ws://13.37.211.176:3001";
// export const url_ws_test = "http://13.37.211.176:3001";