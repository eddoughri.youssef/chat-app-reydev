import React, {useState, createContext, useEffect} from 'react';
import { getCookie, url } from '../UsefullFunction';

export const User = createContext();

function Auth(children){
    return function Index (){
        const [auth, setAuth] = useState(null);
        useEffect(()=>{
            let token = getCookie("token");
            fetch(url + `/user/profile`, {
                method: "GET",
                headers: {
                Authorization: token,
                "Content-Type": "application/json",
                },
            })
            .then((response) => response.json())
            .then((data) => setAuth(data))
            .catch((err) => console.error("error", err));
        },[]);
        return (
            <User.Provider value={auth}>
                {children()}
            </User.Provider>
        )
    }
};

export default Auth;

