import React, { useContext } from 'react';

import {
  createBrowserRouter,
  redirect,
  RouterProvider,
} from "react-router-dom";

import { ThemeProvider, createTheme } from '@mui/material/styles';
import { CssBaseline } from '@mui/material';


import Login from './FirstPage/index.js';
import ChatApp from './ChatApp';
import { getCookie } from './UsefullFunction/index.js';
import { ThemeContext } from './Context';






function App() {
  const themeValue = useContext(ThemeContext); 
  const Theme = createTheme({
    palette: {
      mode: themeValue,
    },
  });


  const router = createBrowserRouter([
  {
    path: "/*",
    element: <ChatApp />,
    loader: ()=>{
      let token = getCookie('token');
      console.log(token)
      if(token!==null)
        return 0;
      return redirect("/login");
    }
  },
  {
    path: "/login",
    element: <Login />,
    loader: ()=>{
      let token = getCookie('token');
      if(token!==null)
        return redirect("/");
      return 0;
    }
  },
  ]);

  return (
    <ThemeProvider theme={Theme}>
      <CssBaseline />
      <RouterProvider router={router} />
    </ThemeProvider>
  )
}

export default App;






