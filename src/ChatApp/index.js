import React, { useState } from "react";

import { Divider, Grid } from "@mui/material";
import { Box } from "@mui/system";

import NavBar from "./NavBar";
import SideBar from "./SideBar";
import MessageSection, {DefaultPage} from "./MessageSection";
import Auth from "../Auth";



function ChatApp() {
  const [chatroom, setChatroom] = useState(null);
  return (
    <>
      <Box>
        <NavBar />
        <Divider />
        <Grid container>
          <Grid xs={12} md={4}>
            <SideBar choseChatroom={setChatroom}/>
          </Grid>
          <Grid xs={12} md={8} sx={{
            height: "100vh",
            alignContent: "flex-start"
          }} container>
            {chatroom === null ? <DefaultPage /> :<MessageSection chatroom={chatroom} />}
          </Grid>
        </Grid>
      </Box>
    </>
  );
}

export default Auth(ChatApp);
