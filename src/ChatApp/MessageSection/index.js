import React, { useContext, useEffect, useState } from "react";
import {
  Grid,
  Divider,
  TextField,
  List,
  ListItem,
  ListItemText,
  Fab,
  Typography,
} from "@mui/material";

import SendIcon from "@mui/icons-material/Send";
import { getCookie, url, url_ws } from "../../UsefullFunction";
import {User} from "../../Auth"


function DefaultPage() {
  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignContent="center"
      alignItems="center"
      xs={12}
    >
      <Typography>Chose A ChatRoom</Typography>
    </Grid>
  );
}

function Index({ chatroom }) {
  const [messages, setMessages] = useState([]);
  const [message, setMessage] = useState(null);
  const user = useContext(User);
  const token = getCookie("token");
  const ws = new WebSocket(url_ws + `/?chatroom=${chatroom.name}&token=${token}`);
  const fetchMessages = () => {
    let token = getCookie("token");
    fetch(url + `/message/${chatroom.name}`, {
      method: "GET",
      headers: {
        Authorization: token,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((data) => setMessages(data))
      .catch((err) => console.error("error", err));
    // document.getElementById("messages").scroll(0,10000000)
  }
  const sendMessage = (message) => {
    let token = getCookie("token");
    console.log("message,", message)
    fetch(url + `/message`, {
      method: "POST",
      headers: {
        Authorization: token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        chatroom: chatroom.name,
        userName: user.name,
        userMessage: message
      })
    })
      .then((response) => response.json())
      .catch((err) => console.error("error", err));
    ws.send(message)
  }
  useEffect(()=>{
      fetchMessages();
      ws.onopen = function(event) {
        console.log("WebSocket is open now.");
      };

      ws.onerror = function(error) {
        console.log("WebSocket error: " + error);
        ws.close();
      };

      ws.onmessage = function(event) {
        fetchMessages();
      };
      return ()=>{
        ws.close();
      }
  }, [chatroom.name]);
  return (
    <>
      <Grid item xs={12}>
        <Typography
          variant="h3"
          backgroundColor={"#1976d2"}
          color="white"
          textAlign="center"
          gutterBottom
          fullWidth
        >
          {chatroom.name}
        </Typography>
      </Grid>
      <Grid id={"messages"} item xs={12}>
        <List
          sx={{
            height: "80vh",
            overflow: "auto",
          }}
        >
          {/* <ListItem key="1">
            <Grid container>
              <Grid item xs={12}>
                <ListItemText
                  align="right"
                  primary={chatroom.name || "unavialbled"}
                ></ListItemText>
              </Grid>
              <Grid item xs={12}>
                <ListItemText align="right" secondary="09:30"></ListItemText>
              </Grid>
            </Grid>
          </ListItem> */}
          {
            messages.map((elem, index)=>{
              return (
                <ListItem key={index+elem.message}>
                  <Grid container>
                    <Grid item xs={12}>
                      <ListItemText align={user?.name === elem.user ? "right" : "left"} secondary={elem.user}></ListItemText>
                    </Grid>
                    <Grid item xs={12}>
                      <ListItemText
                        align={user?.name === elem.user ? "right" : "left"}
                        primary={elem.message}
                      ></ListItemText>
                    </Grid>
                  </Grid>
                </ListItem>
              )
            })
          }
        </List>
        <Divider sx={{ marginTop: "auto" }} />
        <Grid container style={{ padding: "20px", paddingBottom: "0" }}>
          <Grid item xs={11}>
            <TextField
              id="outlined-basic-email"
              label="Message"
              onChange={(event)=>{
                setMessage(event.target.value);
                console.log(message)
              }}
              value={message}
              fullWidth
            />
          </Grid>
          <Grid xs={1} align="right">
            <Fab color="primary" aria-label="add"
                          onClick={()=>{
                            if(message === null || message.length === 0) return;
                            sendMessage(message);
                            setMessage("");
                          }}>
              <SendIcon />
            </Fab>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}

export default Index;

export { DefaultPage };
