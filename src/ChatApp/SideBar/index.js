import React, { useContext, useState } from "react";

import {
  Grid,
  Divider,
  List,
  ListItemIcon,
  ListItemText,
  Avatar,
  ListItemButton,
  Button,
  TextField,
} from "@mui/material";

import { getCookie, url } from "../../UsefullFunction";
import { User } from "../../Auth";

const Chat = ({ choseChatroom }) => {
  const [newChatRoom, setNewChatRoom] = useState(false);
  const [chatrooms, setChatrooms] = useState([]);
  const [name, setName] = useState([]);
  const user = useContext(User);
  console.log(user)
  return (
    <Grid container xs={12}>
      <Grid item xs={12}>
        <ListItemButton key="RemySharp">
          <ListItemIcon>
            <Avatar alt="Remy Sharp" src="" />
          </ListItemIcon>
          <ListItemText primary={user?.name}></ListItemText>
        </ListItemButton>
        <Divider />
        <Grid item xs={12}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              setNewChatRoom(!newChatRoom);
            }}
            sx={{
              borderRadius: "0",
            }}
            fullWidth
          >
            Create Chat Room
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => {
              let token = getCookie("token");
              fetch(url + "/chatroom", {
                method: "GET",
                headers: {
                  Authorization: token,
                  "Content-Type": "application/json",
                },
              })
                .then((response) => response.json())
                .then((data) => setChatrooms(data.map((elem) => elem.name)))
                .catch((err) => console.log("error", err));
            }}
            sx={{
              borderRadius: "0",
            }}
            fullWidth
          >
            Referch
          </Button>
        </Grid>
        {newChatRoom && (
          <Grid
            item
            xs={12}
            sx={{
              margin: {
                xs: "20px 12px",
                md: "20px 20%",
              },
            }}
          >
            <form>
              <TextField
                label="Name"
                variant="outlined"
                value={name}
                onChange={(e) => setName(e.target.value)}
                fullWidth
              />
              <Button
                variant="contained"
                color="success"
                onClick={() => {
                  let token = getCookie("token");
                  fetch(url + "/chatroom", {
                    method: "POST",
                    headers: {
                      Authorization: token,
                      "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                      name: name,
                    }),
                  })
                    .then((response) => response.json())
                    .then((data) => console.log(data))
                    .catch((err) => console.log("error", err));
                }}
                fullWidth
              >
                Commit
              </Button>
            </form>
          </Grid>
        )}
        <Divider />
        <List>
          {/* <ListItemButton key="RemySharp">
                        <ListItemIcon>
                            <Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" />
                        </ListItemIcon>
                        <ListItemText primary="Remy Sharp">Remy Sharp</ListItemText>
                        <ListItemText secondary="online" align="right"></ListItemText>
                    </ListItemButton> */}
          {chatrooms.map((elem, index) => {
            return (
              <ListItemButton
                key={elem + index}
                onClick={() => {
                  choseChatroom({
                    name: elem
                  });
                }}
              >
                <ListItemIcon>
                  <Avatar alt={elem} src="" />
                </ListItemIcon>
                <ListItemText primary={elem}>{elem}</ListItemText>
              </ListItemButton>
            );
          })}
        </List>
      </Grid>
    </Grid>
  );
};

export default Chat;
