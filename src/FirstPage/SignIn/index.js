import React, { useState } from 'react';



import { TextField } from '@mui/material';



function SignIn({email, setEmail, password, setPassword}) {
    return (
        <>
            <TextField
                label="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
                fullWidth
                margin="normal"
            />
            <TextField
                label="Password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
                fullWidth
                margin="normal"
            />
            
        </>
    )
}

export default SignIn;