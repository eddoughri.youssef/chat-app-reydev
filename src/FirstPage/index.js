import React from 'react'

import { useContext } from 'react';

import { Box, Container, Grid } from '@mui/material'

import Login from './Login';
import { ThemeContext } from '../Context';

import Dalle2_light from "../media/DALLE_loginpage.png"
import Dalle2_dark from "../media/DalleDark.png"

function Index() {
    const theme = useContext(ThemeContext);
    return (
        <Grid sx={{
            display:{
                md: "flex",
            },
            justifyContent: 'center',
            overflow:{
                md: "hidden"
            }
        }}>
            <Container 
                sx={{
                    display:{
                        xs: 'none',
                        sm: 'none',
                        md: "block"
                    },
                    flex: "1 1 auto",
                    '&>img':{
                        height: "100vh",
                        width: "50vw",
                        backgroundPosition: "cover"
                    },
                }}>
                    <img 
                        src={theme === 'dark' ? Dalle2_dark : Dalle2_light}
                        alt="ancient king using a chat app"
                    />
                </Container>
            <Container
                sx={{
                    flex: "1 1 auto",
                    height: "100vh",
                    display: "flex",
                    alignItems: "center"
                }}
            >
                <Login />
            </Container>
        </Grid>
  )
}

export default Index;