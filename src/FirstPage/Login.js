import React, {useState} from 'react'



import { Box, Button, Typography, ButtonGroup } from '@mui/material';

import SignIn from './SignIn';
import Signup from './SignUp';


import { useNavigate } from 'react-router-dom';
import { url } from '../UsefullFunction';
const Login = () => {
  const nav = useNavigate();
  const [error, setError] = useState({truth: false, value: null});
  const [actionSign, setAction] = useState(true);

  const [EmailS, setEmailS] = useState('');
  const [passwordS, setPasswordS] = useState('');

  const handleAuth = () => {
    fetch(url + '/user/login', {
        method: 'POST',
        // mode: 'no-cors',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: EmailS,
          password: passwordS
        }),
      }).then(response => response.json()).then(data => {
        const {message, token} = data;
        if(('Email and Password did not match.').match(message) || token === undefined){
          setError({truth: true, value: "Email and Password did not match."});
          return;
        }
        let expiration = new Date();
        expiration.setTime(expiration.getTime() + (60 * 60 * 1000));
        document.cookie = `token=${token}; expires=` + expiration.toUTCString();
        nav("/");
      }).catch(err => console.log("error", err));
  };

  const [usernameR, setUsernameR] = useState('');
  const [emailR, setEmailR] = useState('');
  const [passwordR, setPasswordR] = useState('');


  
  const handleRegistr = () => {
    // Add logic for authentication here
    fetch(url + '/user/register', {
        method: 'POST',
        // mode: 'no-cors',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          name: usernameR,
          email: emailR,
          password: passwordR
        }),
      }).then(response => response.json()).then(data => setError({truth: true, value: data.message, color: 'green'})).catch(err => console.log("error", err));
  };

  return (
    <Box>
      <Typography
        variant='h3'
        sx={{
          '&>span':{
            color: "#f05ef6"
          }
        }}
      ><span>C</span>App</Typography>
      {actionSign ?
            <SignIn email={EmailS} password={passwordS} setEmail={setEmailS} setPassword={setPasswordS}/>
              :
            <Signup username={usernameR} password={passwordR} email={emailR} setEmail={setEmailR} setUsername={setUsernameR} setPassword={setPasswordR}/>}
      {error.truth && <Typography color={error.color || 'red'} fontSize={"0.9rem"}>{error.value}</Typography>}
      <ButtonGroup
          sx={{
              display: 'flex',
              '& > *': {
              m: 1,
              },
          }}
      >
          <Button
            variant="contained"
            color="primary"
            onClick={()=>{
              if(actionSign){
                console.log("add logic to the authentification");
                handleAuth();
              }else{
                console.log("add logic to the Registrging");
                handleRegistr();
              }
            }}
          >
              {actionSign ? "Login": "Create"}
          </Button>
          <Button
            variant="outlined"
            color="secondary"
            onClick={()=>{
              setAction(!actionSign)
            }}>
              {actionSign ? "Register": "Sign In"}
          </Button>
      </ButtonGroup>
    </Box>
  );
};

export default Login;