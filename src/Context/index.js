import { createContext, useState } from 'react';


export const ThemeContext = createContext('light');
export const AuthContext = createContext(null);